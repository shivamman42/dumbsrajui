import { baseEntity } from "./baseEntity";

export class Types extends baseEntity {
    addedDate: string;
    updatedDate: string;
    name: string;
}