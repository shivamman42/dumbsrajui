import { baseEntity } from "./baseEntity";

export class Movies extends baseEntity {
    addedDate: string;
    updatedDate: string;
    name: string;
}