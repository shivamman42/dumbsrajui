import { baseEntity } from "./baseEntity";


export class UserToken extends baseEntity{
    auth_token:string;
    expires_in: string;
    FirstName:string;
    LastName: string;
}