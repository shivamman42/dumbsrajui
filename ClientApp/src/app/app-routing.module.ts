import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from './_guard/auth-guard.service';
import { MovieComponent } from './movie/movie.component';
import { AddMovieComponent } from './movie/add-movie/add-movie.component';
import { EditMovieComponent } from './movie/edit-movie/edit-movie.component';
import { TypeComponent } from './type/type.component';
import { AddTypeComponent } from './type/add-type/add-type.component';
import { EditTypeComponent } from './type/edit-type/edit-type.component';



const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },  
  { path: 'login', component: LoginComponent },
  { path:'dashboard', component:DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'error404', component: ErrorPageComponent },
  {
    path: 'Movies',
    children: [
      { path: "", component: MovieComponent, canActivate: [AuthGuardService] },
      { path: "AddMovie", component: AddMovieComponent, canActivate: [AuthGuardService] },
      { path: "EditMovie", component:EditMovieComponent, canActivate: [AuthGuardService] }
      // { path: "read-article", component: ReadArticleComponent },
    ]
  },
  { path: 'Types', children:[
    {path:'', component: TypeComponent, canActivate: [AuthGuardService] },
    {path:'AddType', component: AddTypeComponent, canActivate: [AuthGuardService] },
    {path:'EditType', component: EditTypeComponent, canActivate: [AuthGuardService] }
  ]},

  { path: '**', redirectTo: '/error404', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
