import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import swal from 'sweetalert';
import { ToastrService } from 'ngx-toastr';
import { Movies } from '../../models/movies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  movies: Movies[];
  constructor(private service: MoviesService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.service.GetAll().subscribe(res => {
      this.movies = res;
    })
  }

  deleteMovie(id) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this imaginary file!",
      icon: "warning",
      dangerMode: true,
      buttons: ["Cancel", true]
      // buttons: true,
      // dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.service.DeleteEntity(id).subscribe(
            () => {
              swal("Category has been deleted!", {
                icon: "success",
              });
              this.getAll();
            },
            error => {
              this.toastr.warning("Error");
            }
          )
        } else {
          swal("Your data is safe!");
        }
      });
  }

  editMovie(id) {
    localStorage.removeItem("movieID");
    localStorage.setItem("movieID", id);
    this.router.navigateByUrl('Movies/EditMovie')
  }
}
