import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MoviesService } from '../../services/movies.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {

  movieForm: FormGroup;
  movieError: Boolean;
  constructor(private formBuilder: FormBuilder, private movieService: MoviesService,
  private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.setForm();
    this.getById();
  }

  getById() {
    var Id = localStorage.getItem("movieID");
    this.movieService.GetById(Id).subscribe(res => {
      // this.typeForm.setValue(res);
      this.movieForm = this.formBuilder.group(
        {
          id: [res.id, Validators.required],
          name: [res.name, Validators.required],
        }
      );
    })
  }


  setForm() {
    this.movieForm = this.formBuilder.group(
      {
        id: ['', Validators.required],
        name: ['', Validators.required],
      }
    );
  }

  onSubmit(value) {
    if (this.movieForm.invalid) {
      this.movieError = true;
      return;
    }
    else {
      // console.log(category);
      this.movieService.UpdateEntity(value).subscribe(
        () => {
          this.toastr.success("Movie Successfully Updated..")
          this.router.navigateByUrl('/Movies');
        },
        error => {
          this.toastr.error("Error: Cannot Update.")
        }
      );
    }
  }

}

