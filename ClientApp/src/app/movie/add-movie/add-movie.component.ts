import { Component, OnInit } from '@angular/core';
import { Movies } from '../../models/movies';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MoviesService } from '../../services/movies.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  movieForm: FormGroup;
  movieError: boolean;

  constructor(private formBuilder: FormBuilder, private toastr: ToastrService,
    private movieService: MoviesService, private router: Router) { }

  ngOnInit() {
    this.setForm();
  }

  setForm() {
    this.movieForm = this.formBuilder.group(
      {
        name: ['', Validators.required],
      }
    );
  }

  onSubmit(value: Movies = new Movies()) {
    if (this.movieForm.invalid) {
      this.movieError = true;
      return;
    }
    else {
      this.movieService.AddEntity(value).subscribe(
        () => {
          this.toastr.success("Movie Successfully Added..")
          this.router.navigateByUrl('/Movies');
        },
        error => {
          this.toastr.error("Error: Cannot Create.")
        }
      );
    }
  }
}

