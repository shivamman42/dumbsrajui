import { Component, OnInit } from '@angular/core';
import { TweenMax } from 'gsap';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errormessage: string = "";
  isLoggedIn: boolean;
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    this.checkLogin();
    this.configureDesign();
  }

  configureDesign() {
    // $('#login-button').click(function () {
    $('#login-button').fadeOut("slow", function () {
      $("#logincontainer").fadeIn();
      TweenMax.from("#logincontainer", .4, { scale: 0 });
      TweenMax.to("#logincontainer", .4, { scale: 1 });
    });
    // });

    $(".close-btn").click(function () {
      TweenMax.from("#logincontainer", .4, { scale: 1 });
      TweenMax.to("#logincontainer", .4, { left: "0px" });
      $("#logincontainer, #forgotten-logincontainer").fadeOut(800, function () {
        $("#login-button").fadeIn(800);
      });
    });
  }

  logInClick(ngForm) {
    this.loginService.logIn(ngForm.value).subscribe(res => {
      if (res && res.auth_token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(res));
        this.isLoggedIn = true
        this.router.navigateByUrl('/dashboard');
      }
    }, error =>{
      this.errormessage = "Invalid Username or Pssword"
    })
  }

  checkLogin(){
    if(localStorage.getItem('currentUser')){
      this.router.navigate['/login']
    }
  }

}
