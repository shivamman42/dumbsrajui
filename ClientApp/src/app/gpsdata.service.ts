// import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
// import { IGps, DataTablesResponse } from './datatable-demo/Gps';
// import { Observable, throwError } from 'rxjs';
// import { catchError } from 'rxjs/operators';
// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class GpsdataService {
//   private _url: string = 'https://localhost:44363/api/DataTableDemo/GetAllGpsData';
//   private httpHeaders: HttpHeaders;
//   constructor(private _http: HttpClient) {
//     this.httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
//   }

//   //getAllGpsdata(): Observable<IGps[]> {
//   //  return this._http.get<IGps[]>(this._url, { headers: this.httpHeaders }).pipe(catchError(this.errorHandler));
//   //}

//   getAllGpsdata(dataTablesParameters) {
//     return this._http.post<DataTablesResponse>(this._url, dataTablesParameters, { headers: this.httpHeaders }).pipe(catchError(this.errorHandler));
//   }

//   errorHandler(error: HttpErrorResponse) {
//     return throwError(error.message || 'Server Error');
//   }
// }
