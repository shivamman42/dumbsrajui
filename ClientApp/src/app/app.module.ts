import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { MaterializeModule } from 'angular2-materialize';
import { ClickOutsideModule } from 'ng-click-outside';
import { NgProgressModule } from '@ngx-progressbar/core';
import {HttpModule} from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
// import { CommonModule } from '@angular/common';  
import { AppRoutingModule } from './/app-routing.module';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FormBuilder} from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { WrapperComponent } from './wrapper/wrapper.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FrontendComponent } from './frontend/frontend.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { HeaderComponent } from './wrapper/header/header.component';
import { FooterComponent } from './wrapper/footer/footer.component';
import { LoadWrapperComponent } from './wrapper/load-wrapper/load-wrapper.component';
import { SideNavComponent } from './wrapper/side-nav/side-nav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MovieComponent } from './movie/movie.component';
import { AddMovieComponent } from './movie/add-movie/add-movie.component';
import { EditMovieComponent } from './movie/edit-movie/edit-movie.component';
import { TypeComponent } from './type/type.component';
import { AddTypeComponent } from './type/add-type/add-type.component';
import { EditTypeComponent } from './type/edit-type/edit-type.component';
import { TypeListComponent } from './type/type-list/type-list.component';
import { MovieListComponent } from './movie/movie-list/movie-list.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    WrapperComponent,
    ErrorPageComponent,
    FrontendComponent,
    AuthComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    LoadWrapperComponent,
    SideNavComponent,
    DashboardComponent,
    MovieComponent,
    AddMovieComponent,
    EditMovieComponent,
    TypeComponent,
    AddTypeComponent,
    EditTypeComponent,
    TypeListComponent,
    MovieListComponent
  ],

  imports: [
    BrowserModule,
    DataTablesModule,
    ClickOutsideModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    MaterializeModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgProgressModule.forRoot()
  ],
  bootstrap: [AppComponent],
  providers:[ { provide: LocationStrategy, useClass: PathLocationStrategy }]
})
export class AppModule { }
