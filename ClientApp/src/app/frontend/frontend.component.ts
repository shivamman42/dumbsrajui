import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
  styleUrls: ['./frontend.component.css']
})
export class FrontendComponent implements OnInit {

  companyName: string;


  constructor() { }

  ngOnInit() {
    this.getCompanyDetails();
    this.getCategories();
    this.getFoods();
  }

  getCompanyDetails() {
   
  }

  getCategories() {
  
  }

  getFoods() {
  }

  getFoodsByCategory(id) {
    
  }

  clearTable(){
  }
}
