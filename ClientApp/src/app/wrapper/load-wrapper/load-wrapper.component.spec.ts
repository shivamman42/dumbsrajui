import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadWrapperComponent } from './load-wrapper.component';

describe('LoadWrapperComponent', () => {
  let component: LoadWrapperComponent;
  let fixture: ComponentFixture<LoadWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
