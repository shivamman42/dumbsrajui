import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { LocationStrategy } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  loggedIn:boolean

  constructor() {

  }

  ngOnInit() {
  }

  checkLogin(){
    if(localStorage.getItem("currentUser") == null){
      this.loggedIn = false
      alert("You are not logged In")
    }
    else{
      this.loggedIn = true
      alert("You are logged in")
    }
  }
}
