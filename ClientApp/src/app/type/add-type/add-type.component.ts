import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TypeService } from '../../services/type.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Type } from '@angular/compiler/src/core';
import { Types } from '../../models/types';

@Component({
  selector: 'app-add-type',
  templateUrl: './add-type.component.html',
  styleUrls: ['./add-type.component.css']
})
export class AddTypeComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private typeService: TypeService, private toastr: ToastrService,
    private router: Router) { }

  typeError: boolean;
  typeForm: FormGroup;
  ngOnInit() {
    this.setForm();
  }

  setForm() {
    this.typeForm = this.formBuilder.group(
      {
        name: ['', Validators.required],
      }
    );
  }

  onSubmit(value: Types = new Types()) {
    if (this.typeForm.invalid) {
      this.typeError = true;
      return;
    }
    else {
      console.log(value)
      this.typeService.AddEntity(value).subscribe(
        () => {
          this.toastr.success("Type Successfully Added..")
          this.router.navigateByUrl('/Types');
        },
        error => {
          this.toastr.error("Error: Cannot Create.")
        }
      );
    }
  }
}
