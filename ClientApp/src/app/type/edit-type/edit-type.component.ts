import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TypeService } from '../../services/type.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-type',
  templateUrl: './edit-type.component.html',
  styleUrls: ['./edit-type.component.css']
})
export class EditTypeComponent implements OnInit {

  Id: string;
  typeForm: FormGroup;
  typeError: boolean;
  constructor(private router: Router, private typeService: TypeService,
    private formBuilder: FormBuilder, private toastr: ToastrService) { }

  ngOnInit() {
    this.setForm();
    this.getById();
  }

  getById() {
    this.Id = localStorage.getItem("typeID");
    this.typeService.GetById(this.Id).subscribe(res => {
      // this.typeForm.setValue(res);
      this.typeForm = this.formBuilder.group(
        {
          id: [res.id, Validators.required],
          name: [res.name, Validators.required],
        }
      );
    })
  }

  setForm() {
    this.typeForm = this.formBuilder.group(
      {
        id: ['', Validators.required],
        name: ['', Validators.required],
      }
    );
  }

  onSubmit(value) {
    if (this.typeForm.invalid) {
      this.typeError = true;
      return;
    }
    else {
      // console.log(category);
      this.typeService.UpdateEntity(value).subscribe(
        () => {
          this.toastr.success("Type Successfully Updated..")
          this.router.navigateByUrl('/Types');
        },
        error => {
          this.toastr.error("Error: Cannot Update.")
        }
      );
    }
  }

}
