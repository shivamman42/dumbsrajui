import { Component, OnInit } from '@angular/core';
import { TypeService } from '../../services/type.service';
import { Types } from '../../models/types';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-type-list',
  templateUrl: './type-list.component.html',
  styleUrls: ['./type-list.component.css']
})
export class TypeListComponent implements OnInit {

  types:Types[]
  constructor(private service: TypeService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.getTypes();
  }

  getTypes(){
    this.service.GetAll().subscribe(res=>{
      this.types = res
    })
  }

  edittype(id){
    localStorage.removeItem("typeID");
    localStorage.setItem("typeID", id);
    this.router.navigateByUrl('/Types/EditType')
  }

  deletetype(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this imaginary file!",
      icon: "warning",
      dangerMode: true,
      buttons: ["Cancel", true]
      // buttons: true,
      // dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.service.DeleteEntity(id).subscribe(
            () => {
              swal("Category has been deleted!", {
                icon: "success",
              });
              this.getTypes();
            },
            error => {
              this.toastr.warning("Error");
            }
          )
        } else {
          swal("Your data is safe!");
        }
      });
  }
}
