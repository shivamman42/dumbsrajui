import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators'
import { throwError } from 'rxjs/internal/observable/throwError';
import { Observable } from 'rxjs/internal/Observable';
import { baseEntity } from '../models/baseEntity';



export class RepositoryService<T extends baseEntity> {

  constructor(protected httpClient: HttpClient,
    private apiUrl: string,
    private endPoint: string)
    { }

    protected getCommonOptions() {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
        })
      };
      return httpOptions;
    } 

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(
        'Something bad happened; please try again later.');
    };

    public AddEntity(entity: T){
      return this.httpClient.post<T>(`${this.apiUrl}/${this.endPoint}/Create`, entity, this.getCommonOptions()).
        pipe(map((data: HttpResponse<T>) => data.body, catchError(this.handleError)));
    }
  
    public UpdateEntity(entity: T){
      return this.httpClient.put<T>(`${this.apiUrl}/${this.endPoint}/update`, entity, this.getCommonOptions()).
        pipe(map((data: HttpResponse<T>) => data.body));
    }
  
    public GetById(id: string): Observable<T> {
      return this.httpClient.get<T>(`${this.apiUrl}/${this.endPoint}/getById/${id}`);
    }
  
    public GetAll(): Observable<T[]> {
      return this.httpClient.get<T[]>(`${this.apiUrl}/${this.endPoint}/list`);
    }
  
    public DeleteEntity(id: string) {
      return this.httpClient.delete(`${this.apiUrl}/${this.endPoint}/delete/${id}`);
    }
}
