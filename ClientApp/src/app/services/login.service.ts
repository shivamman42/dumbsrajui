import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { UserToken } from '../models/userToken';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  httpOptions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };
    return httpOptions;
  }
  
  logIn(value){
    debugger
    var url = environment.apiurl + '/Accounts/login' 
    return  this.httpClient.post<UserToken>(url, value, this.httpOptions());
  }
}
