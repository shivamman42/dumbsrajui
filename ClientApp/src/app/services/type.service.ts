import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RepositoryService } from './repository.service';
import { Types } from '../models/types';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class TypeService  extends RepositoryService<Types>{

  constructor(httpClient: HttpClient) {
    super(
      httpClient,
      environment.apiurl,
      'type');
  }
}
