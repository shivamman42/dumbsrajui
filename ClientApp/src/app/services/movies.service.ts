import { Injectable } from '@angular/core';
import { RepositoryService } from './repository.service';
import { Movies } from '../models/movies';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MoviesService extends RepositoryService<Movies> {

  constructor( httpClient: HttpClient) { 
    super(
      httpClient,
      environment.apiurl,
      'movie');
  }
}
